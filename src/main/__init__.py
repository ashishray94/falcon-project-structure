import falcon
from common import AuthMiddleware

app = application = falcon.API(middleware=[AuthMiddleware()])
from .views import *